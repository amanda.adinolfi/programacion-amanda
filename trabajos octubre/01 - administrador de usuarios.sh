#!/bin/bash

# Amanda Adinolfi
# 20-Octubre-2020
# Es un script para administrar usuarios

LOG=log.txt

echo "Lets create a new user: " | tee $LOG

while :
    do
        read -p -e "New user's name: " NUSER
        if [[ "$NUSER" ~=  "^[^a-zA-Z]" ]]
        then 
            echo "User's name needs to start with a letter" | tee $LOG
        elif [[ "$NUSER" ~=  "[^a-zA-Z0-9]" ]]
            echo "User's name can only contain alphanumeric values" | tee $LOG
        else
            echo "New user's name is $NUSER" | tee $LOG
            break
        fi
    done

while :
    do
        read -p -e "$NUSER email: " NMAIL
        if [[ "$NMAIL" ~=  "\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{1,}\b" ]]
        then 
            echo "User's email is $NMAIL" | tee $LOG
            break
        else
            echo "Email is invalid" | tee $LOG
        fi
    done

useradd -s /bin/bash -d /home/$NUSER -m $NUSER 
echo "New user's password: "
passwd $NUSER 

echo "Creating..." | tee $LOG

echo "New user created" | tee $LOG

echo "Creating user's file" | tee $LOG

echo "$NUSER;$NMAIL" >> created_users.txt

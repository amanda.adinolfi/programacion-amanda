#!/bin/bash

LOG=log.txt
SUDO=''

if [ "$EUID" -ne 0 ]; then
        SUDO='sudo'
fi

echo "Update apt"

$SUDO apt update >> $LOG

echo "Install Apache2"

$SUDO apt install -y apache2 >> $LOG

echo "Apache installed"

$SUDO service apache2 reload

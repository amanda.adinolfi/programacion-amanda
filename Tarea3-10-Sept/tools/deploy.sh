#!/bin/bash

LOG=log.txt
SUDO=''

if [ "$EUID" -ne 0 ]; then
        SUDO='sudo'
fi

echo "Update apt"

$SUDO apt update >> $LOG

echo "Install Apache2"

$SUDO apt install -y apache2 >> $LOG

echo "Install mariadb"

$SUDO apt install -y mariadb-server >> $LOG

echo "Install PHP"

$SUDO apt install -y php libapache2-mod-php php-mysql >> $LOG

$SUDO service apache2 reload >> $LOG
$SUDO service mariadb reload >> $LOG

$SUDO service --status-all >> $LOG

$SUDO touch /var/www/html/info.php >> $LOG

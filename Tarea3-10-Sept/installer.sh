#!/bin/bash

BIN=/opt/bin

sudo mkdir -p $BIN

sudo cp tools/* $BIN

sudo chmod -R 755 $BIN

echo "export PATH=$PATH:$BIN " >> $HOME/.bashrc

source $HOME/.bashrc

#!/bin/bash

# Check if the user is root if not run all commands as sudo

LOG=log.txt
SUDO=''
FPATH=/var/www
FPATH2=/etc/apache2/sites-available

if [ "$EUID" -ne 0 ]; then
        SUDO='sudo'
fi
echo ""

# Check dependencies and install then if missing

echo "Checking if apache2 is installed, if not, install it." | tee $LOG
echo "" | tee $LOG
systemctl status apache2 >> /dev/null
if [ $? -ne 0 ]
then
        echo "" | tee $LOG
        echo "Apache isn't installed, installing now." | tee $LOG
        echo "" | tee $LOG
        install_Apache.sh | tee $LOG
else   
        echo "" | tee $LOG
        echo "Apache already installed" | tee $LOG
fi 

echo "" | tee $LOG

echo "Vhost creation" | tee $LOG

echo ""

echo "It is recommendable that the folder name be the same as the Vhost name"

read -p "Input folder Name: " DIR

echo "Folder name: " $DIR | tee $LOG 

echo "" | tee $LOG 

if $SUDO mkdir $FPATH/"$DIR"
then 
        echo "Folder $DIR created"  | tee $LOG 
else 
        echo "Folder $DIR already exists" | tee $LOG 
fi
echo "" | tee $LOG 

sleep 1

$SUDO chown www-data:www-data /var/www/$SITE/ -R

read -p "Would you like to change the folder owner? [y/n]: " answer
case $answer in
        [yY]* ) read -p "Folder owner's name: " owner 
                $SUDO chown $owner:$owner $FPATH/"$DIR" -R
                ;;

        [nN]* ) ;;

        * )     echo "Please choose Y or N.";;
esac

echo "" | tee $LOG 

sleep 1

echo "index.html for Vhost" | tee $LOG 
echo "" | tee $LOG 

read -p "Please input a file name for the site: " fileName 

echo "Putting content on the site" | tee $LOG 

sudo dd of=$FPATH/"$DIR"/"$fileName" << EOF
<html> 
  <head> 
    <title> Header </title> 
  </head> 
  <body> 
    <h1> Hello world </h1> 
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
  </body> 
</html>
EOF

echo "Folder contains: " | tee $LOG 

ls -ld $FPATH/"$DIR" | tee $LOG 
echo "" | tee $LOG 

sleep 1

#################################CONFIG VHOST
echo "Config Vhost" | tee $LOG 
echo "" | tee $LOG 

sudo dd of=$FPATH2/"$DIR".conf << EOF
<VirtualHost *:80>
ServerName $DIR 

ServerAdmin webmaster@localhost
DocumentRoot $FPATH/"$DIR"

ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
EOF

echo "Vhost configured" | tee $LOG 
echo "" | tee $LOG 

echo "Enabling Site"  | tee $LOG 

$SUDO ln -s $FPATH2/"$DIR".conf /etc/apache2/sites-enabled/"$DIR".conf 

echo "Sites Enabled: "| tee $LOG 

ls -l /etc/apache2/sites-enabled | tee $LOG 
echo "" | tee $LOG 

sleep 1

# Restart Apache2

read -p "Would you like to restart Apache? [y/N] " answer
case $answer in
        [yY]* ) echo "Restarting Apache..." | tee $LOG 
                $SUDO systemctl reload apache2
                ;;

        [nN]* ) echo "Apache will not be restarted" | tee $LOG 
                ;;

        * )     echo "Please choose Y or N.";;
esac

sleep 1

###################### Config hosts file

echo "Configuring /etc/hosts" | tee $LOG 

echo "

127.0.0.1       $DIR

" >> $SUDO /etc/hosts

echo "" | tee $LOG 

############ Curl install

echo "Checkinf if curl is already installed" | tee $LOG 

echo "" | tee $LOG 

systemctl status curl >> /dev/null
if [ $? -ne 0 ]
then
    echo "Installing Curl" | tee $LOG 

    $SUDO apt-get install curl -y >> /dev/null

    echo "Curl installed" | tee $LOG 

else
    echo "Curl is already installed" | tee $LOG 
fi

echo "" | tee $LOG 

########################################## Website Status

echo "Checking Site Status" | tee $LOG 

echo "" | tee $LOG 

curl -Is http://$DIR | head -1  | tee $LOG 

sleep 1

echo "Uninstalling Curl" | tee $LOG 

$SUDO apt-get --purge remove curl -y

echo "Curl Uninstalled" | tee $LOG 

######################## Services Check

echo "Active Services" | tee $LOG 

echo "" | tee $LOG 

service --status-all | tee $LOG 

echo "" | tee $LOG 
echo "Script completed!" | tee $LOG 
echo "" | tee $LOG 

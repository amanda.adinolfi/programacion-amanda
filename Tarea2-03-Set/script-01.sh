#!/bin/bash

FNAME = "$1"
PER = $2

mkdir $1

echo "Folder " $FNAME " created."

chmod $PER $FNAME

echo "Permission " $PER " granted to folder " $FNAME

ls -l $FNAME

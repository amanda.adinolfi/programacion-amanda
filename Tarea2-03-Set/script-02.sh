#!/bin/bash

read -p "New user's name: " NUSER

useradd -s /bin/bash -d /home/$NUSER -m $NUSER 

echo "New user's password: "
passwd $NUSER 
echo ""

read -p "What is the group that $NUSER will be part of: " GROUP
addgroup $GROUP
echo "" 
usermod -a -G  $GRUPO $NUSER

echo "User $NUSER created and added to group $GROUP"
